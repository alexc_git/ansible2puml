#!/usr/bin/env python
'''Transform an Ansible site directory structure into PlantUML

Requires Python 3.

usage:
    [~/ansible/site/]$ ~/ansible2puml.py > site.puml

TODO:
    * Refactor so Ansible2Puml and PUmlClass have clear responsiblities
    * Breakup classes into libs
'''
import os, re, yaml, sys
from pprint     import *
from copy       import *
from functools  import reduce
from itertools  import filterfalse

class PUmlCommon:
    '''Shared functionality among all PUml objects'''
    def __init__(s): pass

    def _indent(s, lvl=1): return ' ' * 3 * lvl

    def indent_str(s, string):
        '''Return an indented version of string'''
        return '\n'.join( [ s._indent() + i 
                                for i in string.split('\n') ] )

    def load_yml(s, filename):
        '''Returns data structure of yml payload in filename'''
        yml = None;
        with open(filename, 'r') as stream:
            try:
                yml = yaml.load(stream)
                #pprint.pprint(yml)
            except yaml.YAMLError as exc:
                print(' YAML error: '.center(20,'='))
                print(exc)
                sys.exit(1)
        assert yml is not None, 'file %s was not loaded' % filename
        return yml

class PUmlClass(PUmlCommon):
    '''Object reprsentation of PUML classes which are Ansible config files

    Args:
        name    (str)   : the name of the Ansible file/PUML class
        is_role (bool)  : mark file as role, not include or etc.

    Attributes:
        name        (str)   : the name arg
        is_role     (bool)  : the is_role arg
        tree_dlm    (str)   : the delimiter to signify tree branch.
                              Defaults to '+'.
        dirstruct   (dict)  : nested dict containing dir struct and files
        files       (list)  : 
        vars        (list)  :
    '''
    def __init__(s, name, is_role=False):
        s.name      = name or 'unnamed'
        s.is_role   = is_role
        s.tree_dlm  = '+'
        s.dirstruct = {}
        s.files     = []
        s.vars      = []
        s.role_incs = {}
        s.task_incs = {}
        # for introspection a la Data::Dumper
        s.d = { k : eval('s.' + k, {'s':s} ) 
                    for k in 'name is_role tree_dlm dirstruct files' 
                             ' vars role_incs task_incs'.split() }

    def __str__(s): return pformat(s.d)

    def get_dirstruct_str(s):
        return s._proc_dirstruct(s.dirstruct).rstrip()

    def get_files(s):
        if not s.files : 
            s._proc_dirstruct(s.dirstruct).rstrip()
        return s.files

    def _proc_dirstruct(s, dirstruct, lvl=0):
        '''Recursively process dirstruct
        
        Return:
            dirstruct_str (str): dirstruct as str
        '''
        if not dirstruct   : return ''
        if lvl             : s.files[-1] += '/' 

        path   = s.files[-1] if len(s.files) else ''
        string = ''
        cnt    = 0
        for k in sorted(dirstruct):
            if not lvl : 
                s.files.append(k)
            else : 
                if cnt  : s.files.append(path)
                s.files[-1] += k
            cnt += 1

            string += ( s._indent(lvl)
                        + ( s.tree_dlm + k if lvl else k )
                        + '\n' )
            string += s._proc_dirstruct(dirstruct[k], lvl+1)

        return string

    def get_vars(s):
        '''Return all corresponding yaml config for variables'''
        all_vars = []
        reg_vars = []
        if not s.vars: 
            for f in s.get_files():
                yml = str( s.load_yml('./roles/%s/%s' % (s.name, f)) )
                all_vars += re.findall(r'{{\s*([^(]*?)\s*}}', yml)
                reg_vars += re.findall(r"register': '(.*?)'", yml)

            all_vars = { re.sub(r'(\..*|\s.*)$', '', v) for v in all_vars }
            all_vars = all_vars.difference( set(reg_vars) | {'item', 'hostvars'} )
            s.vars   = list(all_vars)
        return s.vars

    def puml(s):
        dirstruct = s.get_dirstruct_str()
        if dirstruct:
            dirstruct = '\n'.join( ['{method} .' + i 
                                        for i in dirstruct.split('\n')] )
            dirstruct = s.indent_str(dirstruct)

        all_vars = [ '{field} ' + v for v in s.get_vars() ]
        all_vars.sort()
        all_vars = '\n'.join(all_vars)
        
        d = [ 'class %s {' % s.name, all_vars, dirstruct, '}' ]
        return '\n'.join(d)

class Ansible2Puml(PUmlCommon):
    '''Translate a Ansible standard configuration directory structure into
        PlantUML formatted output'''
    def __init__(s):
        PUmlCommon.__init__(s)
        s.dir_root = './roles'

    def roledir2PUml(s):
        '''Return a list of PUmlClass objects for each role in ./roles/'''
        dir     = {} # dict of dict
        rootdir = s.dir_root
        rootdir = rootdir.rstrip(os.sep)    # remove tail /
        start   = rootdir.rfind(os.sep) + 1 # find the last /
        # build dir, dict of dict
        for path, dirs, files in os.walk(rootdir):
            folders = path[start:].split(os.sep) # del './', path as list
            subdir  = dict.fromkeys(files)
            *parent_path, new_dir = folders
            # ['a', 'path'] to dir[a][path]
            parent          = reduce(dict.get, parent_path, dir) 
            parent[new_dir] = subdir # add subdir to dic

        uml_classes = []
        for role in dir['roles'].keys():
            uml_classes.append( PUmlClass(role, True) )
            uml_classes[-1].dirstruct = deepcopy(dir['roles'][role] )
        return uml_classes

    def filter_files(s, files):
        '''Return a list of only interesting files'''
        regex = re.compile(r'\.(swp|retry|j2)|(^hosts)$')
        return list( filterfalse(regex.search, files) )

    def get_ansible_incs(s, filename):
        '''Get lists of role and task includes from yaml config file'''
        role_inc_all = []
        task_inc_all = []
        for play in s.load_yml(filename):
            if type(play) is dict:
                roles = play.get('roles',[])
                role_inc_all += roles
                tasks    = play.get('tasks',[])
                task_inc = [d['include'] for d in tasks if 'include' in d]
                task_inc_all += task_inc
    
        return (role_inc_all, task_inc_all)

    def inc2puml(s, object_name, role_incs, task_incs):
        '''Convert yaml task and role includes to puml'''
        nameDelim = '::';
        puml = []
        for v in role_incs:
            puml.append( object_name + ' --> ' + v)

        uniq = {}
        for v in task_incs:
            uniq[ re.match(r'^roles\/([^/]*)', v).group(1) ] = None

        [ puml.append( object_name + ' --> '  + i) for i in sorted(uniq) ]
        return '\n'.join(puml).rstrip()

    def print_puml(s):
        '''Print plantuml formatted text from Ansible yaml'''
        print('@startuml')
        print('set namespaceSeparator ::\n')
        role_classes = s.roledir2PUml()

        print('package roles {')
        for i in role_classes: 
            print( s.indent_str( i.puml() ) )
        print('}\n')

        files = s.filter_files([
                        f for f in os.listdir('.') if os.path.isfile(f) ])
        for f in files:
            print( PUmlClass(f).puml() )
            
            r_inc, t_inc = s.get_ansible_incs(f)
            incpuml = s.inc2puml(f, r_inc, t_inc)
            if incpuml: print(incpuml + '\n')

        print('@enduml')
###########################################################################

Ansible2Puml().print_puml()
