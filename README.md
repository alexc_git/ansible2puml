# ansible2puml.py #

Transform an Ansible site directory structure into PlantUML

### SYNOPSIS ###

	[~/ansible/site/]$ ~/ansible2puml.py > site.puml
	
### EXAMPLE ###

#### Structure ####
```
example/lamp_simple/
├── group_vars
│   ├── all
│   └── dbservers
├── hosts
├── roles
│   ├── common
│   │   ├── handlers
│   │   │   └── main.yml
│   │   └── tasks
│   │       └── main.yml
│   ├── db
│   │   ├── handlers
│   │   │   └── main.yml
│   │   └── tasks
│   │       └── main.yml
│   └── web
│       ├── handlers
│       │   └── main.yml
│       └── tasks
│           ├── copy_code.yml
│           ├── install_httpd.yml
│           └── main.yml
└── site.yml
```

#### Output PUML ####
```
@startuml
set namespaceSeparator ::

package roles {
   class db {
   {field} dbname
   {field} dbuser
   {field} mysql_port
   {field} upassword
      {method} .handlers
      {method} .   +main.yml
      {method} .tasks
      {method} .   +main.yml
   }
   class web {
   {field} httpd_port
   {field} repository
      {method} .handlers
      {method} .   +main.yml
      {method} .tasks
      {method} .   +copy_code.yml
      {method} .   +install_httpd.yml
      {method} .   +main.yml
   }
   class common {
   
      {method} .handlers
      {method} .   +main.yml
      {method} .tasks
      {method} .   +main.yml
   }
}

class site.yml {


}
site.yml --> common
site.yml --> web
site.yml --> db

@enduml
```

#### Output PNG ####
![Alt](https://bytebucket.org/alexc_git/ansible2puml/raw/b86ae35270ee90b09d2b8d109909d9c03eabfaad/example/lamp_simple.png puml)